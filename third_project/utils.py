from crm.models import Booking,Landlord,Tenant,Apartment,Settings
from dateutil.rrule import *
from datetime import datetime, date
from datetime import timedelta
from django.db.models import Q
from django.db.models import Sum


def str_to_bool(s):
    if s == 'True':
        return True
    elif s == 'False':
        return False

def convert_lists_to_str(arr_list):
    data = ''       
    for j in arr_list:
        arr_str = ','.join(j)
        data = data + arr_str +'\r\n'
    return data

def has_empty_fields(d):
    for key in d:
        if len(str(d[key]))<1:
            return True
    return False

def create_context(pk):
    deposit_day = rrule(DAILY, byweekday=(MO,TU,WE,TH,FR), dtstart=date.today())[Landlord.objects.get(id=pk).turnaround]
    deposit_day_formatted = (deposit_day.strftime( '%a, %B %d'))
    landlord = Landlord.objects.get(id=pk).name
    conditions = Settings.objects.all()
    booking = Booking.objects.filter(apartment__landlord=Landlord.objects.get(id=pk))
    isHoliday = conditions.get(id=1)
    dr = Booking.objects.filter(Q(apartment__landlord=Landlord.objects.get(id=pk)) & Q(payment_type__iexact='Debit')).aggregate(Sum('rent'))['rent__sum'] #Q is imported from db.models and allows to combine multiple filters on a query (use "&" as AND, "|" as OR)
    cr = Booking.objects.filter(Q(apartment__landlord=Landlord.objects.get(id=pk)) & Q(payment_type__iexact='Credit')).aggregate(Sum('rent'))['rent__sum']

    if dr:
        dr_formatted = '{0:,.2f}'.format(dr)
    else:
        dr_formatted = '{0:,.2f}'.format(0)

    if cr:
        cr_formatted = '{0:,.2f}'.format(cr)
    else:
        cr_formatted = '{0:,.2f}'.format(0)
        

    context = {'isHoliday': isHoliday,
            'landlord':landlord,
            'booking':booking,
            'debits':dr_formatted,
            'credits':cr_formatted,
            'deposit_day':deposit_day_formatted
    }

    return context

