import requests
import json
from third_project.utils import convert_lists_to_str

def create_eft_batch(payload):

    url = 'https://api.na.bambora.com/v1/batchpayments'
    data = "--CHEESE\r\nContent-Disposition: form-data; name=\"criteria\"\r\nContent-Type: application/json\r\n\r\n{{\r\n    \"process_date\": \"{}\",\r\n    \"sub_merchant_id\": \"{}\"\r\n}}\r\n--CHEESE\r\nContent-Disposition: form-data; name=\"{}\"; filename=\"{}.csv\"\r\nContent-Type: text/plain\r\n\r\n{}--CHEESE--\r\n".format(payload['process_date'], payload['sub_merchant_id'], payload['filename'], payload['filename'], convert_lists_to_str(payload['transactions']))

    headers = {
        'Content-Type': 'multipart/form-data; boundary=CHEESE',
        'Authorization': 'Passcode {}'.format(payload['batch_api_key']),
        'FileType': 'STD',
    }
  
    response = requests.request("POST", url, headers=headers, data = data)
    print(response.json())

    return response



