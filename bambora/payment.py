import requests
import json
import time
from config import settings


def make_payment(amount, payment_method, customer_code, card_id, complete):

    headers = {
        'Authorization': 'Passcode {}'.format(settings["payment_api_key"]),
        'Content-Type': 'application/json',
    }

    data = {"amount":0,"payment_method":"","payment_profile":{"customer_code":"","card_id":"","complete":""}}
    data["amount"]= amount
    data["payment_method"]= payment_method
    data["payment_profile"]["customer_code"]= customer_code
    data["payment_profile"]["card_id"]= card_id
    data["payment_profile"]["complete"]= complete

    response = requests.post('https://api.na.bambora.com/v1/payments', headers=headers, data=json.dumps(data))
    transaction_id = response.json()["id"]
    # print(response.json())

    return transaction_id
    

def search_transaction(transaction_id):

    headers = {
        'Authorization': 'Passcode {}'.format(settings["payment_api_key"]),
        'Accept': 'application/json',
    }

    response = requests.get('https://api.na.bambora.com/v1/payments/{}'.format(transaction_id), headers=headers)
    transaction_details = response.json()
    # print(transaction_details)

    return transaction_details


# make_payment(225,"payment_profile","95bB871Bee864Dd49e99E5712C621FA0","1","true")
# x = search_transaction(10000005)
# print(x["amount"])
# print(x["card"]["name"])
# print(x["created"])
