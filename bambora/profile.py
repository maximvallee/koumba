import requests
import json
import time
from bambora.config import settings
# from config import settings
from tokenize import tokenize

def create_eft_profile(language,comments,bank_account_holder,account_number,bank_account_type,institution_number,branch_number):

    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Passcode {}'.format(settings["profiles_api_key"]),
    }

    data = { "language":"", "comments":"", "bank_account":{ "bank_account_holder":"", "account_number":"", "bank_account_type":"", "institution_number":"", "branch_number":"" }}

    data["language"]= language
    data["comments"] = comments
    data["bank_account"]["bank_account_holder"] = bank_account_holder
    data["bank_account"]["account_number"] = account_number
    data["bank_account"]["bank_account_type"] = bank_account_type
    data["bank_account"]["institution_number"] = institution_number
    data["bank_account"]["branch_number"] = branch_number

    response = requests.post('https://api.na.bambora.com/v1/profiles', headers=headers, data=json.dumps(data))
    print(response.json())
    customer_code = response.json()["customer_code"]
    
    return customer_code

# create_eft_profile("en","","John Malcovich","123456","Canadian","123","12773")


def create_cc_profile(name, number, expiry_month, expiry_year, cvd):

    headers = {
        'Authorization': 'Passcode {}'.format(settings["profiles_api_key"]),
        'Content-Type': 'application/json',
    }
    # import pdb; pdb.set_trace()

    data = {"token":{"name":"","code":""}}
    data["token"]["name"] = name
    data["token"]["code"] = tokenize(number, expiry_month, expiry_year, cvd)

    response = requests.post('https://api.na.bambora.com/v1/profiles', headers=headers, data=json.dumps(data))
    print(response.json())
    customer_code = response.json()["customer_code"]

    return customer_code

# print(create_cc_profile("John Vineo","371100001000131","02", "20", "1234"))









def update_or_create_profile(payload):

    # url = "https://api.na.bambora.com/v1/profiles/{}".format(payload['customer_code'])
    url = "https://api.na.bambora.com/v1/profiles"

    headers = {
        'Authorization': 'Passcode {}'.format(payload['profiles_api_key']),
        'Content-Type': 'application/json',
        'Cookie': 'ASP.NET_SessionId=5b3xk455bqpxbrmsf4d3h155'
    }

    # import pdb; pdb.set_trace()
    if payload['customer_code']:
        method = "PUT"
    else:
        method = "POST"
    import pdb; pdb.set_trace()
    response = requests.request(method, url, headers=headers, data = json.dumps(payload['data']))
    print(response.json())
    
    return response






def get_profile(payload):
    
    url = "https://api.na.bambora.com/v1/profiles/{}".format(payload['customer_code'])

    headers = {
        'Authorization': 'Passcode {}'.format(payload['profiles_api_key']),
        'Content-Type': 'application/json',
        'Cookie': 'ASP.NET_SessionId=5b3xk455bqpxbrmsf4d3h155',
        'Sub-Merchant-Id': payload["sub_merchant_id"]
    }

    response = requests.request("GET", url, headers=headers, data = payload)
    print(response.json())
    profile_info = response.json()

    return profile_info

def update_profile(payload):
    url = "https://api.na.bambora.com/v1/profiles/{}".format(payload['customer_code'])

    headers = {
        'Authorization': 'Passcode {}'.format(payload['profiles_api_key']),
        'Content-Type': 'application/json',
        'Cookie': 'ASP.NET_SessionId=5b3xk455bqpxbrmsf4d3h155',
        'Sub-Merchant-Id': payload["sub_merchant_id"]
    }

    # import pdb; pdb.set_trace()
    response = requests.request("PUT", url, headers=headers, data = json.dumps(payload['data']))
    print(response.json())
    
    return response

def delete_profile(payload):

    url = "https://api.na.bambora.com/v1/profiles/{}".format(payload['customer_code'])

    headers = {
        'Authorization': 'Passcode {}'.format(payload["profiles_api_key"]),
        'Content-Type': 'application/json',
        'Cookie': 'ASP.NET_SessionId=5b3xk455bqpxbrmsf4d3h155',
        'Sub-Merchant-Id': payload["sub_merchant_id"]
    }
    # import pdb; pdb.set_trace()
    response = requests.request("DELETE", url, headers=headers, data = {})
    print(response.json())

    return response

def create_profile(payload):

    url = "https://api.na.bambora.com/v1/profiles"

    headers = {
        'Authorization': 'Passcode {}'.format(payload['profiles_api_key']),
        'Content-Type': 'application/json',
        'Cookie': 'ASP.NET_SessionId=5b3xk455bqpxbrmsf4d3h155',
        'Sub-Merchant-Id': payload["sub_merchant_id"]
    }

    # import pdb; pdb.set_trace()
    response = requests.request("POST", url, headers=headers, data = json.dumps(payload['data']))
    print(response.json())
    
    return response

def delete_card(payload):
    # import pdb; pdb.set_trace()
    url = "https://api.na.bambora.com/v1/profiles/{}/cards/1".format(payload['customer_code'])

    headers = {
        'Authorization': 'Passcode {}'.format(payload['profiles_api_key']),
        'Content-Type': 'application/json',
        'Sub-Merchant-Id': payload["sub_merchant_id"]
    }

    response = requests.request("DELETE", url, headers=headers, data = payload['data'])
    print(response.json())
    
    return response

def update_card(payload):
    # import pdb; pdb.set_trace()
    url = "https://api.na.bambora.com/v1/profiles/{}/cards/1".format(payload['customer_code'])

    headers = {
        'Authorization': 'Passcode {}'.format(payload['profiles_api_key']),
        'Content-Type': 'application/json',
        'Sub-Merchant-Id': payload["sub_merchant_id"]
    }

    response = requests.request("PUT", url, headers=headers, data = json.dumps(payload['data']))
    print(response.json())
    
    return response

def create_card(payload):

    url = "https://api.na.bambora.com/v1/profiles/{}/cards".format(payload['customer_code'])

    headers = {
        'Authorization': 'Passcode {}'.format(payload['profiles_api_key']),
        'Content-Type': 'application/json',
        'Sub-Merchant-Id': payload["sub_merchant_id"]
    }

    response = requests.request("POST", url, headers=headers, data = json.dumps(payload['data']))
    print(response.json())
    
    return response
