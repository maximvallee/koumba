import requests
import json
import time
from config import settings

def tokenize(number, expiry_month, expiry_year, cvd):
    
    headers = {
        'Content-Type': 'application/json',
    }

    data = { "number":"", "expiry_month":"", "expiry_year":"", "cvd":"" }
    data["number"] = number
    data["expiry_month"] = expiry_month
    data["expiry_year"] = expiry_year
    data["cvd"] = cvd

    response = requests.post('https://api.na.bambora.com/scripts/tokenization/tokens', headers=headers, data=json.dumps(data))
    token = response.json()["token"]
    # print(response.json())

    return token

# print(tokenize("4030000016666321","02", "20", "123"))


















