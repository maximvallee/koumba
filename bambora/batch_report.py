import requests

def get_batch(payload):

    url = "https://web.na.bambora.com/scripts/reporting/report.aspx"

    payload = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<request>\r\n<rptVersion>2.0</rptVersion>\r\n<serviceName>BatchPaymentsEFT</serviceName>\r\n<merchantId>{}</merchantId>\r\n \
        <passCode>{}</passCode>\r\n<rptFormat>JSON</rptFormat>\r\n<rptFilterBy1>batch_id</rptFilterBy1>\r\n<rptOperationType1>EQ</rptOperationType1>\r\n<rptFilterValue1>{}</rptFilterValue1>\r\n \
        <rptMerchantId>{}</rptMerchantId>\r\n</request>".format(payload['merchant_id'], payload['report_passcode'], payload['batch_id'], payload['sub_merchant_id'])
    headers = {
    'Content-Type': 'application/xml',
    'Cookie': 'ASP.NET_SessionId=p0n2q355pnrnb4njgvmwq355'
    }

    response = requests.request("POST", url, headers=headers, data = payload)
    print(response.text.encode('utf8'))

    return response



