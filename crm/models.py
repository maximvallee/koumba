from django.db import models
from django.urls import reverse
from datetime import date
from django.utils import timezone
from djmoney.models.fields import MoneyField

# Create your models here.
#Apartments (nickname, address, unit, booking )

# class Settings(models.Model):
#     isHoliday = models.BooleanField(default=False)
#     holiday_message = models.TextField(max_length=200)

class Settings(models.Model):
    setting = models.CharField(max_length=100)
    status = models.BooleanField(default=False)
    value = models.TextField(max_length=300)

    def get_absolute_url(self):
        return reverse("setting_list")
    
    class Meta(object):
        verbose_name_plural = 'Settings'

class Apartment(models.Model):
    landlord = models.ForeignKey('Landlord', on_delete=models.CASCADE)
    nickname = models.CharField(max_length=100)
    address = models.CharField(max_length=100)
    unit = models.CharField(max_length=100)

    def get_absolute_url(self):
        return reverse("apartment_list")

    def __str__(self):
        return self.unit+ "-" + self.address

#Landlords (apartment, address, name, email, phone)
class Landlord(models.Model):
    business = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    office = models.CharField(max_length=100)
    email =models.EmailField(max_length=100)
    phone = models.CharField(max_length=20)
    turnaround = models.IntegerField()
    sub_merchant_id = models.CharField(max_length=9, blank=True) ############################ INTEGRATIOIN

    def get_absolute_url(self):
        return reverse("landlord_list")

    def __str__(self):
        return self.business

#Tenants (booking, name, email, phone)
class Tenant(models.Model):

    PROVINCES_CHOICES= [
        ('AB', 'Alberta'),
        ('BC', 'British Columbia'),
        ('MB', 'Manitoba'),
        ('NB', 'New Brunswick'),
        ('NL', 'Newfoundland and Labrador'),
        ('NT', 'Northwest Territories'),
        ('NS', 'Nova Scotia'),
        ('NU', 'Nunavut'),
        ('ON', 'Ontario'),
        ('NB', 'Prince Edward Island'),
        ('QC', 'Quebec'),
        ('SK', 'Saskatchewan'),
        ('YT', 'Yukon'),
    ]

    COUNTRIES_CHOICES= [
        ('CA','Canada'),
    ]

    ACCOUNT_TYPES = [
        ('CA','Canadian'),
    ]

    YEARS_CHOICES= [
        ('', '--'),
        (abs(date.today().year) % 100, date.today().year),
        (abs(date.today().year+1) % 100, date.today().year +1),
        (abs(date.today().year+2) % 100, date.today().year +2),
        (abs(date.today().year+3) % 100, date.today().year +3),
        (abs(date.today().year+4) % 100, date.today().year +4),
        (abs(date.today().year+5) % 100, date.today().year +5),
        (abs(date.today().year+6) % 100, date.today().year +6),
        (abs(date.today().year+7) % 100, date.today().year +7),
        (abs(date.today().year+8) % 100, date.today().year +8),
        (abs(date.today().year+9) % 100, date.today().year +9),
        (abs(date.today().year+10) % 100, date.today().year +10),
    ]

    MONTHS_CHOICES= [
        ('', '--'),
        ('01', '01'),
        ('02', '02'),
        ('03', '03'),
        ('04', '04'),
        ('05', '05'),
        ('06', '06'),
        ('07', '07'),
        ('08', '08'),
        ('09', '09'),
        ('10', '10'),
        ('11', '11'),
        ('12', '12')
    ]

    CUSTOMER_TYPES = [
        ('Tenant','Tenant'),
        ('Service provider','Service provider')
    ]

    customer_type = models.CharField(max_length=20, blank=False, choices=CUSTOMER_TYPES)
    full_name = models.CharField(max_length=45)
    email = models.EmailField(max_length=45, unique=True)
    phone = models.CharField(max_length=18, unique=True)
    address = models.CharField(max_length=75, blank=True)
    city = models.CharField(max_length=75, blank=True)
    province = models.CharField(max_length=2, blank=True, choices=PROVINCES_CHOICES)
    postal_code = models.CharField(max_length=6, blank=True)
    country = models.CharField(max_length=2, blank=True, choices=COUNTRIES_CHOICES)

    bank_account_holder = models.CharField(max_length=50, blank=True)
    bank_account_type = models.CharField(max_length=2, choices=ACCOUNT_TYPES, default='CA', blank=True)
    institution_number = models.CharField(max_length=3, blank=True)
    branch_number = models.CharField(max_length=5, blank=True)
    account_number= models.CharField(max_length=12, blank=True)

    card_name = models.CharField(max_length=50, blank=True)
    card_number = models.CharField(max_length=16, blank=True)
    card_expiry_month = models.CharField(max_length=2, blank=True)
    card_expiry_year = models.CharField(max_length=2, blank=True)
    card_cvd = models.CharField(max_length=5, blank=True)


    def get_absolute_url(self):
        return reverse("tenant_list")

    def __str__(self):
        return self.full_name

class Booking(models.Model):

    STATUS_OPTIONS = [
        ('Pending', 'Pending'),
        ('In-tenancy', 'In-tenancy'),
        ('Checked-out', 'Checked-out'),
    ]


    tenant = models.ForeignKey('Tenant', on_delete=models.CASCADE)
    apartment = models.ForeignKey('Apartment', on_delete=models.CASCADE)
    check_in = models.DateField(blank=False)
    check_out = models.DateField(blank=False)
    # rent = models.DecimalField(max_digits=5, decimal_places=2, blank=False)
    rent = MoneyField(max_digits=14, decimal_places=2, default_currency='CAD')
    profile = models.ForeignKey('Profile', on_delete=models.CASCADE)
    status = models.CharField(max_length = 15, choices=STATUS_OPTIONS)

    def save(self, *args, **kwargs):
        # import pdb; pdb.set_trace()
        if  date.today() < self.check_in:
            self.status = 'Pending'
        elif date.today() >= self.check_in and date.today() <= self.check_out:
            self.status = 'In-tenancy'
        else:
            self.status = 'Checked-out'

        self.rent = round(self.rent, 2)
        super(Booking, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse("booking_list")

    def __str__(self):
        return 'Booking ({})'.format(self.tenant.full_name)

class Profile(models.Model):
    tenant = models.ForeignKey('Tenant', on_delete=models.CASCADE)
    landlord = models.ForeignKey('Landlord', on_delete=models.CASCADE)
    # booking = models.ForeignKey('Booking', on_delete=models.CASCADE)
    customer_code = models.CharField(max_length=50, blank=False)

    def __str__(self):
        return 'Profile ({})'.format(self.customer_code)

class Payment(models.Model):

    PAYMENT_METHODS = [
        ('DD', 'Direct deposit'),
        ('CC', 'Credit card'),
    ]

    PAYMENT_TYPE = [
        ('D', 'Collect money'),
        ('C', 'Send money'),
    ]


    # booking = models.ForeignKey('Booking', on_delete=models.CASCADE)
    tenant = models.ForeignKey('Tenant', on_delete=models.CASCADE)
    batch = models.ForeignKey('Batch', on_delete=models.CASCADE)
    # amount = models.DecimalField(max_digits=5, decimal_places=2, blank=False)
    amount = MoneyField(max_digits=14, decimal_places=2, default_currency='CAD')
    payment_type = models.CharField(max_length=1, blank=False, choices=PAYMENT_TYPE)
    payment_method = models.CharField(max_length=2, blank=False, choices=PAYMENT_METHODS)


class Batch(models.Model):

    STATUS_CHOICES = [
        ('Pending', 'Pending'),
        ('Sent', 'Sent'),
    ]

    created = models.DateTimeField(auto_now_add=True)
    process_date = models.DateField(null=False, default=timezone.now())
    settlement_date = models.DateField(null=True)
    landlord = models.ForeignKey('Landlord', on_delete=models.CASCADE)
    confirmation_code = models.CharField(max_length=50, blank=True)
    status = models.CharField(max_length=7, default='Pending', choices=STATUS_CHOICES)

    # payment_type = models.CharField(max_length=1, blank=False, choices=PAYMENT_TYPE)
    # booking = models.ManyToManyField(Booking)

    class Meta(object):
        verbose_name_plural = 'Batches'



########## BAMBORA TESTING

# class Profile(models.Model):

#     PROVINCES_CHOICES= [
#         ('AB', 'Alberta'),
#         ('BC', 'British Columbia'),
#         ('MB', 'Manitoba'),
#         ('NB', 'New Brunswick'),
#         ('NL', 'Newfoundland and Labrador'),
#         ('NT', 'Northwest Territories'),
#         ('NS', 'Nova Scotia'),
#         ('NU', 'Nunavut'),
#         ('ON', 'Ontario'),
#         ('NB', 'Prince Edward Island'),
#         ('QC', 'Quebec'),
#         ('SK', 'Saskatchewan'),
#         ('YT', 'Yukon'),
#     ]

#     COUNTRIES_CHOICES= [
#         ('CA','Canada'),
#     ]

#     # customer_code = models.CharField(max_length=50, unique=True, blank=False)
#     full_name = models.CharField(max_length=45)
#     email = models.EmailField(max_length=45, unique=True)
#     phone = models.CharField(max_length=18, unique=True)
#     address = models.CharField(max_length=75, blank=True)
#     city = models.CharField(max_length=75, blank=True)
#     province = models.CharField(max_length=2, blank=True, choices=PROVINCES_CHOICES)
#     postal_code = models.CharField(max_length=6, blank=True)
#     country = models.CharField(max_length=2, blank=True, choices=COUNTRIES_CHOICES)


#     def __str__(self):
#         return self.full_name

# class BankAccount(models.Model):

#     ACCOUNT_TYPES = [
#         ('CA','Canadian'),
#         ('US', 'American')
#     ]

#     customer_code = models.OneToOneField(Profile, on_delete=models.CASCADE)
#     bank_account_holder = models.CharField(max_length=50)
#     bank_account_type = models.CharField(max_length=2, choices=ACCOUNT_TYPES, default='CA')
#     institution_number = models.CharField(max_length=3)
#     branch_number = models.CharField(max_length=5)
#     account_number= models.CharField(max_length=12, unique=True)

#     def __str__(self):
#         return str(self.customer_code)