from django import forms
from crm.models import Apartment,Landlord,Tenant,Booking,Settings,Profile,Payment,Batch
from django.conf import settings
from django.forms import DateInput
from datetime import date
from django.core.validators import RegexValidator
from django.utils import timezone
from djmoney.models.fields import MoneyField

from django.contrib.auth.forms import AuthenticationForm ### Custom LoginView
from django.forms.widgets import PasswordInput, TextInput ### Custom LoginView

class LoginForm(AuthenticationForm):
    username=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control', 'id':'inputEmail', 'placeholder':'Username', 'autofocus':'autofocus'}))
    password=forms.CharField(widget=PasswordInput(attrs={'class':'form-control', 'id':'inputPassword', 'placeholder':'Email', 'autofocus':'autofocus'}))

class SettingsForm(forms.ModelForm):
    class Meta():
        model = Settings
        fields = ('setting','status', 'value')

class ApartmentForm(forms.ModelForm):
    class Meta():
        model = Apartment
        fields = ('landlord','nickname','address','unit')

class LandlordForm(forms.ModelForm):
    class Meta():
        model = Landlord
        fields = '__all__'

class TenantForm(forms.ModelForm):
    class Meta():
        model = Tenant
        fields = 'customer_type', 'full_name', 'email', 'phone', 'address', 'city', 'province', 'postal_code', 'country'


# class BatchForm(forms.Form):

#     PAYMENT_TYPE = [
#         ('D', 'Collect money'),
#         ('C', 'Send money'),
#     ]

#     def __init__(self, *args, **kwargs):
#         super(BatchForm, self).__init__(*args, **kwargs)
#         # import pdb; pdb.set_trace()
#         self.fields['landlord'] = forms.ChoiceField(
#             choices=[(landlord.id, str(landlord)) for landlord in Landlord.objects.exclude(sub_merchant_id__exact='')]
#         )
#         self.fields['booking'] = forms.MultipleChoiceField(
#             choices=[(booking.id, str(booking)) for booking in Booking.objects.all()]
#         )

#         self.fields['booking'].widget.attrs={'id': booking.apartment.landlord.id for booking in Booking.objects.all()}

        
#     process_date = forms.DateField(widget=forms.DateInput(attrs={'type': 'date'}), initial=timezone.now())
#     payment_type = forms.CharField(max_length=1, widget=forms.Select(choices= PAYMENT_TYPE))
#     status = forms.CharField(initial='Pending', widget=forms.HiddenInput())


    # landlord = forms.


    # customer_code = forms.CharField(label='Customer Code', widget=forms.HiddenInput())
    # is_new = forms.BooleanField(label='New Card', widget=forms.HiddenInput())
    # card_name = forms.CharField(label='Cardholder', max_length=75)
    # card_number = forms.CharField(label='Card number', min_length=8, max_length=19)
    # card_expiry_month = forms.CharField(label='Expiry month', widget=forms.Select(choices= MONTHS_CHOICES))
    # card_expiry_year = forms.CharField(label='Expiry year', required=True, widget=forms.Select(choices= YEARS_CHOICES))
    # card_cvd = forms.CharField(label='CVD', max_length=5)

# class BatchForm(forms.ModelForm):
#     process_date = forms.DateField(widget=forms.DateInput(attrs={'type': 'date'}), initial=timezone.now())

#     def __init__(self, *args, **kwargs):
#         super(BatchForm, self).__init__(*args, **kwargs)
#         ## Display only 'live' merchants
#         self.fields['landlord'].queryset = Landlord.objects.exclude(sub_merchant_id__exact='')

#     class Meta():
#         model = Batch
#         fields = ('process_date', 'landlord', 'payment_type', 'booking')


class BatchForm(forms.Form):

    PAYMENT_TYPE = [
        ('D', 'Collect money'),
        ('C', 'Send money'),
    ]

    process_date = forms.DateField(widget=forms.DateInput(attrs={'type': 'date'}), initial=timezone.now())
    payment_type = forms.CharField(max_length=1, widget=forms.Select(choices= PAYMENT_TYPE))
    # landlord = forms.ChoiceField(required=True)
    landlord = forms.ChoiceField(label="Landlords")
    


    def __init__(self, *args, **kwargs):
        super(BatchForm, self).__init__(*args, **kwargs)
        
        self.fields['landlord'] = forms.ChoiceField(
            choices=[(landlord.id, str(landlord)) for landlord in Landlord.objects.exclude(sub_merchant_id__exact='')]
        )
        self.fields['landlord'].choices.insert(0, ('','--------'))
        # import pdb; pdb.set_trace()

        try:
            # self.fields['booking'] = forms.MultipleChoiceField(choices=[(booking.id, str(booking)) for booking in Booking.objects.filter(apartment__landlord=Landlord.objects.get(id=self.data['landlord']))])
            self.fields['tenant'] = forms.MultipleChoiceField(choices=[(tenant.id, tenant) for tenant in Tenant.objects.filter(booking__apartment__landlord=Landlord.objects.get(id=self.data['landlord']))])
                
            self.fields['landlord'].widget.attrs['readonly'] = True
        except:
            pass






class BookingForm(forms.ModelForm):

    check_in = forms.DateField(widget=forms.DateInput(attrs={'type': 'date'}))
    check_out = forms.DateField(widget=forms.DateInput(attrs={'type': 'date'}))
    # apartment = forms.ChoiceField(label="Apartments")
    
    class Meta():
        model = Booking
        fields = ('check_in', 'check_out', 'tenant', 'apartment', 'rent')
    
    def __init__(self, *args, **kwargs):
        super(BookingForm, self).__init__(*args, **kwargs)
        # import pdb; pdb.set_trace()
        
        ## Display only apartments managed by the landlord associated to the tenant's customer_code
        # self.fields['apartment'] = forms.ChoiceField(
        #     choices=[(apartment.id, apartment) for apartment in kwargs['instance'].apartment.landlord.apartment_set.all()]
        # )


class PaymentForm(forms.ModelForm):

    class Meta():
        model = Payment
        fields = ('amount', 'payment_type', 'payment_method')

# class ProfileForm(forms.Form):

#     LANGUAGES_CHOICES= [
#         ('en','English'),
#         ('fr', 'French')
#     ]

#     PROVINCES_CHOICES= [
#         ('AB', 'Alberta'),
#         ('BC', 'British Columbia'),
#         ('MB', 'Manitoba'),
#         ('NB', 'New Brunswick'),
#         ('NL', 'Newfoundland and Labrador'),
#         ('NT', 'Northwest Territories'),
#         ('NS', 'Nova Scotia'),
#         ('NU', 'Nunavut'),
#         ('ON', 'Ontario'),
#         ('NB', 'Prince Edward Island'),
#         ('QC', 'Quebec'),
#         ('SK', 'Saskatchewan'),
#         ('YT', 'Yukon'),
#     ]



#     COUNTRIES_CHOICES= [
#         ('CA','Canada'),
#         ('US', 'United States')
#     ]

#     customer_code = forms.CharField(label='Customer Code', widget=forms.HiddenInput())
#     language = forms.CharField(label='Language', required=False, widget=forms.Select(choices= LANGUAGES_CHOICES))
#     billing_name = forms.CharField(label='Name', max_length=45)
#     billing_phone_number = forms.CharField(label='Phone', min_length=8, max_length=18)
#     billing_email_address = forms.EmailField(label='Email', max_length=45)
#     billing_address_line1 = forms.CharField(label='Address', max_length=75, required=False)
#     billing_address_line2 = forms.CharField(label='Address 2', max_length=75, required=False)
#     billing_city = forms.CharField(label='City', max_length=45, required=False)
#     billing_province = forms.CharField(label='Province', required=False, widget=forms.Select(choices= PROVINCES_CHOICES))
#     billing_country = forms.CharField(label='Country', required=False, widget=forms.Select(choices= COUNTRIES_CHOICES))
#     billing_postal_code = forms.CharField(label='Postal Code', max_length=6, required=False)


# class ProfileForm2(forms.ModelForm):
#     customer_code=forms.CharField(required=False, widget=forms.HiddenInput())

#     class Meta():
#         model = Profile
#         fields = ('customer_code','full_name', 'email', 'phone', 'address', 'city', 'province', 'postal_code', 'country')



# class CreditCardForm(forms.Form):

#     YEARS_CHOICES= [
#         ('', '--'),
#         (abs(date.today().year) % 100, date.today().year),
#         (abs(date.today().year+1) % 100, date.today().year +1),
#         (abs(date.today().year+2) % 100, date.today().year +2),
#         (abs(date.today().year+3) % 100, date.today().year +3),
#         (abs(date.today().year+4) % 100, date.today().year +4),
#         (abs(date.today().year+5) % 100, date.today().year +5),
#         (abs(date.today().year+6) % 100, date.today().year +6),
#         (abs(date.today().year+7) % 100, date.today().year +7),
#         (abs(date.today().year+8) % 100, date.today().year +8),
#         (abs(date.today().year+9) % 100, date.today().year +9),
#         (abs(date.today().year+10) % 100, date.today().year +10),
#     ]

#     MONTHS_CHOICES= [
#         ('', '--'),
#         ('01', '01'),
#         ('02', '02'),
#         ('03', '03'),
#         ('04', '04'),
#         ('05', '05'),
#         ('06', '06'),
#         ('07', '07'),
#         ('08', '08'),
#         ('09', '09'),
#         ('10', '10'),
#         ('11', '11'),
#         ('12', '12')
#     ]

#     customer_code = forms.CharField(label='Customer Code', widget=forms.HiddenInput())
#     is_new = forms.BooleanField(label='New Card', widget=forms.HiddenInput())
#     card_name = forms.CharField(label='Cardholder', max_length=75)
#     card_number = forms.CharField(label='Card number', min_length=8, max_length=19)
#     card_expiry_month = forms.CharField(label='Expiry month', widget=forms.Select(choices= MONTHS_CHOICES))
#     card_expiry_year = forms.CharField(label='Expiry year', required=True, widget=forms.Select(choices= YEARS_CHOICES))
#     card_cvd = forms.CharField(label='CVD', max_length=5)

# class BankAccountForm(forms.Form):

#     ACCOUNT_TYPES = [
#         ('CA','Canadian'),
#         ('US', 'American')
#     ]
#     customer_code = forms.CharField(label='Customer Code', widget=forms.HiddenInput())
#     bank_account_holder = forms.CharField(label='Bank account holder', max_length=45)
#     bank_account_type = forms.CharField(label='Account type', widget=forms.Select(choices= ACCOUNT_TYPES, attrs={'disabled': 'disabled'}))
#     bank_institution_number = forms.CharField(label='Institution number', max_length=3, min_length=3)
#     bank_branch_number = forms.CharField(label='Branch number', max_length=5, min_length=5)
#     bank_account_number = forms.CharField(label='Bank account holder', max_length=12, min_length=6)

class BankAccountForm(forms.ModelForm):

    ACCOUNT_TYPES = [
        ('CA','Canadian'),
    ]

    bank_account_holder=forms.CharField(required=True)
    bank_account_type=forms.CharField(widget=forms.Select(choices= ACCOUNT_TYPES))
    institution_number=forms.CharField(required=True, max_length=3)
    branch_number=forms.CharField(required=True, max_length=5)
    account_number=forms.CharField(required=True, max_length=12)

    class Meta():
        model = Tenant
        fields = ('bank_account_holder', 'bank_account_type', 'institution_number', 'branch_number', 'account_number')
        # , 'bank_account_holder', 'bank_account_type', 'institution_number', 'branch_number', 'account_number'

class CreditCardForm(forms.ModelForm):

    YEARS_CHOICES= [
        ('', '--'),
        (abs(date.today().year) % 100, date.today().year),
        (abs(date.today().year+1) % 100, date.today().year +1),
        (abs(date.today().year+2) % 100, date.today().year +2),
        (abs(date.today().year+3) % 100, date.today().year +3),
        (abs(date.today().year+4) % 100, date.today().year +4),
        (abs(date.today().year+5) % 100, date.today().year +5),
        (abs(date.today().year+6) % 100, date.today().year +6),
        (abs(date.today().year+7) % 100, date.today().year +7),
        (abs(date.today().year+8) % 100, date.today().year +8),
        (abs(date.today().year+9) % 100, date.today().year +9),
        (abs(date.today().year+10) % 100, date.today().year +10),
    ]

    MONTHS_CHOICES= [
        ('', '--'),
        ('01', '01'),
        ('02', '02'),
        ('03', '03'),
        ('04', '04'),
        ('05', '05'),
        ('06', '06'),
        ('07', '07'),
        ('08', '08'),
        ('09', '09'),
        ('10', '10'),
        ('11', '11'),
        ('12', '12')
    ]

    is_new = forms.BooleanField(initial=True)
    card_name = forms.CharField(label='Cardholder', required=True, max_length=75)
    card_number = forms.CharField(label='Card number',required=False, min_length=8, max_length=19)
    card_expiry_month = forms.CharField(label='Expiry month', required=True, widget=forms.Select(choices= MONTHS_CHOICES))
    card_expiry_year = forms.CharField(label='Expiry year', required=True, widget=forms.Select(choices= YEARS_CHOICES))
    card_cvd = forms.CharField(label='CVD', required=False, max_length=5)

    class Meta():
        model = Tenant
        fields = ('card_name', 'card_number', 'card_expiry_month', 'card_expiry_year', 'card_cvd')
    
    def __init__(self, *args, **kwargs):
        super(CreditCardForm, self).__init__(*args, **kwargs)
        # import pdb; pdb.set_trace()
        instance = getattr(self, 'instance', None)
        if len(instance.card_number) > 0:
            self.fields['card_number'].disabled = True
            self.fields['card_cvd'].disabled = True
            instance.is_new = False
        # if self.fields['card_number']

    # customer_code = forms.CharField(label='Customer Code', widget=forms.HiddenInput())
    # bank_account_holder = forms.CharField(label='Bank account holder', max_length=45)
    # bank_account_type = forms.CharField(label='Account type', widget=forms.Select(choices= ACCOUNT_TYPES, attrs={'disabled': 'disabled'}))
    # bank_institution_number = forms.CharField(label='Institution number', max_length=3, min_length=3)
    # bank_branch_number = forms.CharField(label='Branch number', max_length=5, min_length=5)
    # bank_account_number = forms.CharField(label='Bank account holder', max_length=12, min_length=6)



