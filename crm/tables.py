import django_tables2 as tables
from .models import Apartment


class ApartmentTable(tables.Table):
    class Meta:
        model = Apartment
        template_name = 'django_tables2/bootstrap4.html'
        row_attrs = {
            'data-id': lambda record: record.pk
        }
