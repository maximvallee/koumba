from django.contrib import admin
from crm.models import Apartment,Landlord,Tenant,Booking,Settings,Profile,Profile,Payment,Batch

# class TenantAdmin(admin.ModelAdmin):
#     list_display =['id','name', 'last_name', 'email', 'phone']
    # list_editable = ['name', 'last_name', 'email', 'phone', 'apartment']

# class BookingAdmin(admin.ModelAdmin):
#     list_display =['id','profile', 'tenant', 'apartment', 'check_in', 'check_out']

# class LandlordAdmin(admin.ModelAdmin):
#     list_display =['id','business', 'sub_merchant_id']

# # class SettingsAdmin(admin.ModelAdmin):
# #     list_display =['isHoliday', 'holiday_message']

# class SettingsAdmin(admin.ModelAdmin):
#     list_display =['id','setting', 'status', 'value']

# class ProfileAdmin(admin.ModelAdmin):
#     list_display =['id','customer_code']

class BatchAdmin(admin.ModelAdmin):
    list_display =['id','created']



# Register your models here.
admin.site.register(Apartment)
admin.site.register(Landlord)
admin.site.register(Tenant)
admin.site.register(Booking)
admin.site.register(Settings)
admin.site.register(Profile)
admin.site.register(Payment)
admin.site.register(Batch,BatchAdmin)

