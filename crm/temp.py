######VIEWS.PY#####

def load_profile_form(request, pk):
    # import pdb; pdb.set_trace()
    profile = get_profile(pk)

    # try:
    # import pdb; pdb.set_trace()
    form = ProfileForm({
        'customer_code': pk,
        'language': profile['language'],
        'billing_name': profile['billing']['name'],
        'billing_address_line1' : profile['billing']['address_line1'],
        'billing_address_line2' : profile['billing']['address_line2'],
        'billing_city' : profile['billing']['city'],
        'billing_province' : profile['billing']['province'],
        'billing_country' : profile['billing']['country'],
        'billing_postal_code' : profile['billing']['postal_code'],
        'billing_phone_number' : profile['billing']['phone_number'],
        'billing_email_address' : profile['billing']['email_address'],
        'card_name' : profile['card']['name'],
        # 'card_number' : profile['card']['number'],
        'card_number' : '4030000010001234',
        'card_expiry_month' : profile['card']['expiry_month'],
        'card_expiry_year' : profile['card']['expiry_year'],
        # 'card_cvd' : '',
        'card_cvd' : '123'
        }) 
    # except:
    #     form = ProfileForm()
    
    return render(request, 'crm/profile_form.html', {'form': form})


def ProfileSaveView(request):
    if request.method == "POST":
        import pdb; pdb.set_trace()
    
        data = { 
            "language": request.POST.get("language"),
            "comments":"", 
            "billing": {
                "name": request.POST.get("billing_name"), 
                "address_line1": request.POST.get("billing_address_line1"),
                "address_line2": request.POST.get("billing_address_line2"),
                "city": request.POST.get("billing_city"),
                "province": request.POST.get("billing_province"),
                "country": request.POST.get("billing_country"),
                "postal_code": request.POST.get("billing_postal_code"),
                "phone_number": request.POST.get("billing_phone_number"),
                "email_address": request.POST.get("billing_email_address")
            },
            "card": {
                "name": request.POST.get("card_name"),
                "number": request.POST.get("card_number"),
                "expiry_month": request.POST.get("card_expiry_month"),
                "expiry_year": request.POST.get("card_expiry_year"),
                "cvd": request.POST.get("card_cvd")
            },
            "bank_account":{
                "bank_account_holder": request.POST.get("bank_account_holder"),
                "account_number": request.POST.get("bank_account_number"),
                "bank_account_type": 'CA',
                "institution_number": request.POST.get("bank_institution_number"),
                "branch_number": request.POST.get("bank_branch_number")
            }
        }

        payload = {
            'profiles_api_key': settings["profiles_api_key"],
            'customer_code': request.POST.get("customer_code"),
            'data': data
        }

        attempt_num = 0  # keep track of how many times we've retried
        while attempt_num < 2:

            response = update_profile(payload)

            if response.status_code == 200:
                data = response.json()
                return redirect('/bambora/batch/list')
                # return render(request, 'crm/batch_list.html')
                # return Response(data, status=status.HTTP_200_OK)
            else:
                attempt_num += 1
                # print(response.status_code)
                # You can probably use a logger to log the error here
                time.sleep(5)  # Wait for 5 seconds before re-trying
        # return Response({"error": "Request failed"}, status=r.status_code)
        return HttpResponse("Error {}: {}".format(response.status_code, response.reason))
    else:
        # return Response({"error": "Method not allowed"}, status=status.HTTP_400_BAD_REQUEST)
        return HttpResponse({"error": "Method not allowed"})


######FORMS.PY#####



class ProfileForm(forms.Form):

    LANGUAGES_CHOICES= [
        ('en','English'),
        ('fr', 'French')
    ]

    PROVINCES_CHOICES= [
        ('AB', 'Alberta'),
        ('BC', 'British Columbia'),
        ('MB', 'Manitoba'),
        ('NB', 'New Brunswick'),
        ('NL', 'Newfoundland and Labrador'),
        ('NT', 'Northwest Territories'),
        ('NS', 'Nova Scotia'),
        ('NU', 'Nunavut'),
        ('ON', 'Ontario'),
        ('NB', 'Prince Edward Island'),
        ('QC', 'Quebec'),
        ('SK', 'Saskatchewan'),
        ('YT', 'Yukon'),
    ]

    MONTHS_CHOICES= [
        ('01', '01'),
        ('02', '02'),
        ('03', '03'),
        ('04', '04'),
        ('05', '05'),
        ('06', '06'),
        ('07', '07'),
        ('08', '08'),
        ('09', '09'),
        ('10', '10'),
        ('11', '11'),
        ('12', '12')
    ]

    COUNTRIES_CHOICES= [
        ('CA','Canada'),
        ('US', 'United States')
    ]

    ACCOUNT_TYPES = [
        ('CA','Canadian'),
        ('US', 'American')
    ]

    YEARS_CHOICES= [
        (abs(date.today().year) % 100, date.today().year),
        (abs(date.today().year+2) % 100, date.today().year +2),
        (abs(date.today().year+3) % 100, date.today().year +3),
        (abs(date.today().year+4) % 100, date.today().year +4),
        (abs(date.today().year+5) % 100, date.today().year +5),
        (abs(date.today().year+6) % 100, date.today().year +6),
        (abs(date.today().year+7) % 100, date.today().year +7),
        (abs(date.today().year+8) % 100, date.today().year +8),
        (abs(date.today().year+9) % 100, date.today().year +9),
        (abs(date.today().year+10) % 100, date.today().year +10),
    ]

    # customer_code = forms.CharField(label='Customer Code', widget=forms.TextInput(attrs={'readonly' : True}))
    customer_code = forms.CharField(label='Customer Code', widget=forms.HiddenInput())
    language = forms.CharField(label='Language', required=False, widget=forms.Select(choices= LANGUAGES_CHOICES))
    billing_name = forms.CharField(label='Name', max_length=45, required=False)
    billing_address_line1 = forms.CharField(label='Address', max_length=75, required=False)
    billing_address_line2 = forms.CharField(label='Address 2', max_length=75, required=False)
    billing_city = forms.CharField(label='City', max_length=45, required=False)
    billing_province = forms.CharField(label='Province', required=False, widget=forms.Select(choices= PROVINCES_CHOICES))
    billing_country = forms.CharField(label='Country', required=False, widget=forms.Select(choices= COUNTRIES_CHOICES))
    billing_postal_code = forms.CharField(label='Postal Code', max_length=6, required=False)
    billing_phone_number = forms.CharField(label='Phone', min_length=8, max_length=18, required=False)
    billing_email_address = forms.EmailField(label='Email', max_length=45, required=False)
    card_name = forms.CharField(label='Cardholder', max_length=75, required=False)
    card_number = forms.CharField(label='Card number', min_length=8, max_length=19, required=False)
    card_expiry_month = forms.CharField(label='Expiry month', required=False, widget=forms.Select(choices= MONTHS_CHOICES))
    card_expiry_year = forms.CharField(label='Expiry year', required=False, widget=forms.Select(choices= YEARS_CHOICES))
    card_cvd = forms.CharField(label='CVD', max_length=5, required=False)
    bank_account_holder = forms.CharField(label='Bank account holder', max_length=45, required=False)
    bank_account_type = forms.CharField(label='Account type', required=False, widget=forms.Select(choices= ACCOUNT_TYPES, attrs={'disabled': 'disabled'}))
    bank_institution_number = forms.CharField(label='Institution number', max_length=3, min_length=3, required=False)
    bank_branch_number = forms.CharField(label='Branch number', max_length=5, min_length=5, required=False)
    bank_account_number = forms.CharField(label='Bank account holder', max_length=12, min_length=6, required=False)
