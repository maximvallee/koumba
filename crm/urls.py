from django.urls import path
from . import views
# from crm.views import apartments

urlpatterns = [
    path('',views.DashboardView,name='dashboard'),

    path('settings/',views.SettingsListView.as_view(),name='setting_list'),
    path('settings/<int:pk>/edit', views.SettingsUpdateView.as_view(), name='setting_edit'),
    path('settings/new/', views.SettingsCreateView.as_view(), name='setting_new'),

    path('apartment/',views.ApartmentListView.as_view(),name='apartment_list'),
    path('apartment/<int:pk>/edit', views.ApartmentUpdateView.as_view(), name='apartment_edit'),
    path('apartment/new/', views.ApartmentCreateView.as_view(), name='apartment_new'),
    path('apartment/<int:pk>/delete', views.ApartmentDeleteView.as_view(), name='apartment_delete'),

    path('landlord/',views.LandlordListView.as_view(),name='landlord_list'),
    path('landlord/<int:pk>/edit', views.LandlordUpdateView.as_view(), name='landlord_edit'),
    path('landlord/new/', views.LandlordCreateView.as_view(), name='landlord_new'),
    path('landlord/<int:pk>/delete', views.LandlordDeleteView.as_view(), name='landlord_delete'),
    path('landlord/<int:pk>/sample', views.SampleStatementView.as_view(), name='landlord_sample_statement'),

    path('tenant/',views.TenantListView.as_view(),name='tenant_list'),
    path('tenant/<int:pk>/edit', views.TenantUpdateView.as_view(), name='tenant_edit'),
    path('tenant/new/', views.TenantCreateView.as_view(), name='tenant_new'),
    path('tenant/<str:pk>/edit/bank-account', views.BankAccountUpdateView.as_view(), name='bank_account_update'),
    path('tenant/<str:pk>/edit/credit-card', views.CreditCardUpdateView.as_view(), name='credit_card_update'),
    path('tenant/<int:pk>/delete', views.TenantDeleteView.as_view(), name='tenant_delete'),
    path('tenant/upload', views.tenant_upload, name='tenant_upload'),

    path('booking/',views.BookingListView.as_view(),name='booking_list'),
    path('booking/<int:pk>/edit', views.BookingUpdateView.as_view(), name='booking_edit'),
    path('booking/new/', views.BookingCreateView.as_view(), name='booking_new'),


    path('batch/',views.BatchListView.as_view(),name='batch_list'),
    # path('batch/new/', views.create_batch, name='batch_new'),
    path('batch/new/', views.create_batch, name='batch_new_1'),
    path('batch/<int:pk>/review/', views.BatchDetailsView.as_view(), name='batch_review'),
    path('batch/<int:pk>/send/', views.send_batch, name='batch_send'),
    path('batch/<int:pk>/delete', views.BatchDeleteView.as_view(), name='batch_delete'),

    path('payment/<int:pk>/edit/', views.PaymentUpdateView.as_view(), name='payment_update'),




 ]
