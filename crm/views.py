from django.shortcuts import render, redirect
from django.views.generic import View,TemplateView,ListView,DetailView,UpdateView,CreateView,DeleteView
from .models import Apartment,Landlord,Tenant,Booking,Settings,Profile,Batch,Payment
from django.urls import reverse_lazy, reverse
from .forms import ApartmentForm,LandlordForm,TenantForm,BookingForm,SettingsForm, CreditCardForm, BankAccountForm, BatchForm, PaymentForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
import time
import re, string

from django_tables2 import RequestConfig
from .tables import ApartmentTable
# Create your views here.

#CSV UPLOAD IMPORTS
import csv # Used to import CSV
import io # Used for working with io streams (streams of data)
from django.contrib import messages #To display a message prompt
from third_project.utils import create_context, str_to_bool, has_empty_fields

from bambora.config import settings
from bambora.profile import get_profile, delete_profile, update_or_create_profile, delete_card, update_card, create_card, create_profile, update_profile
from bambora.batch import create_eft_batch
from bambora.batch_report import get_batch

@login_required
def DashboardView(request):
    count_apartment = Apartment.objects.all().count()
    count_landlord = Landlord.objects.all().count()
    count_tenant= Tenant.objects.all().count()
    count_booking= Booking.objects.all().count()
    context = {'count_apartment':count_apartment,'count_landlord':count_landlord, 'count_tenant':count_tenant, 'count_booking':count_booking }
    return render(request,'crm/dashboard.html', context)

# SETTINGS
class SettingsListView(LoginRequiredMixin,ListView):
    login_url = '/accounts/login/'
    context_object_name = 'settings'
    model = Settings

class SettingsUpdateView(LoginRequiredMixin,UpdateView):
    login_url = '/accounts/login/'
    form_class = SettingsForm
    model = Settings

class SettingsCreateView(LoginRequiredMixin,CreateView):
    login_url = '/accounts/login/'
    form_class = SettingsForm
    model = Settings


# APARTMENT

class ApartmentListView(LoginRequiredMixin,ListView):
    login_url = '/accounts/login/'
    context_object_name = 'apartments'
    model = Apartment

    #ENTRY COUNT: get_context_data allows us to inject extra content into class-based views. Here we want to inject number of records, which is not in the model, but calculated
    def get_context_data(self,**kwargs):
        context = super(ApartmentListView,self).get_context_data(**kwargs)
        context['count_apartment'] = Apartment.objects.all().count()
        return context

class ApartmentUpdateView (LoginRequiredMixin,UpdateView):
    login_url = '/accounts/login/'
    form_class = ApartmentForm
    model = Apartment

class ApartmentCreateView(LoginRequiredMixin,CreateView):
    login_url = '/accounts/login/'
    form_class = ApartmentForm
    model = Apartment

class ApartmentDeleteView(LoginRequiredMixin,DeleteView):
    login_url = '/accounts/login/'
    model = Apartment
    success_url = reverse_lazy('apartment_list')


# LANDLORD

class LandlordListView(LoginRequiredMixin,ListView):
    login_url = '/accounts/login/'
    context_object_name = 'landlords'
    model = Landlord

    def get_context_data(self,**kwargs):
        context = super(LandlordListView,self).get_context_data(**kwargs)
        context['count_landlord'] = Landlord.objects.all().count()
        return context

class LandlordUpdateView (LoginRequiredMixin,UpdateView):
    login_url = '/accounts/login/'
    form_class = LandlordForm
    model = Landlord

class LandlordCreateView(LoginRequiredMixin,CreateView):
    login_url = '/accounts/login/'
    form_class = LandlordForm
    model = Landlord

class LandlordDeleteView(LoginRequiredMixin,DeleteView):
    login_url = '/accounts/login/'
    model = Landlord
    success_url = reverse_lazy('landlord_list')

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        success_url = self.get_success_url()
        fail_url = 'landlord_delete'
        existing_profiles = self.object.profile_set.all()
        # import pdb; pdb.set_trace()

        ### Delete both on local and Bambora ###
        if existing_profiles:
            for profile in existing_profiles:
                payload = {
                    'profiles_api_key': settings["profiles_api_key"],
                    'sub_merchant_id': profile.landlord.sub_merchant_id,
                    'customer_code': profile.customer_code,
                }

                response = delete_profile(payload)

                if response.status_code != 200:
                    message = "Error: {}".format(response.json())
                    messages.info(self.request, message)
                    return redirect(fail_url, pk=self.object.id)

            self.object.delete()
            return HttpResponseRedirect(success_url)

        ### Delete on local only (does not exist on Bambora)###
        else:
            self.object.delete()
            return HttpResponseRedirect(success_url)

class SampleStatementView(TemplateView):
    template_name = "sample_statement.html"
    
    def get_context_data(self,**kwargs):
        context = super(SampleStatementView,self).get_context_data(**kwargs)
        context = create_context(self.kwargs['pk'])
        return context


# TENANT

class TenantListView(LoginRequiredMixin,ListView):
    login_url = '/accounts/login/'
    context_object_name = 'tenants'
    model = Tenant

    def get_context_data(self,**kwargs):
        context = super(TenantListView,self).get_context_data(**kwargs)
        context['count_tenant'] = Tenant.objects.all().count()
        return context

@login_required
def tenant_upload(request):
    if request.method == "GET": #Pulling the tenant_upload page and form when someone calls the function view
        return render(request,"tenant_upload.html")

    csv_file = request.FILES['file'] #We grab the file from the form using the 'file' name tag (in template)
    if not csv_file.name.endswith('.csv'): #We check if the file is .csv exention, and  display prompt message if not
        messages.error(request,'This is not a CSV file')

    data_set = csv_file.read().decode('UTF-8') #This is from the csv import. Allows us to read the csv file.
    io_string = io.StringIO(data_set) #Returns a stream of strings for reading or writing
    next(io_string) #Skips the first line of the io_string file
    for column in csv.reader(io_string, delimiter=',',quotechar='|'):
        _, created = Tenant.objects.update_or_create(
            name=column[0],
            last_name=column[1],
            email=column[2],
            phone=column[3]
        )
    context={}
    return render(request, "tenant_confirm_upload.html", context)

class TenantUpdateView (LoginRequiredMixin,UpdateView):
    login_url = '/accounts/login/'
    form_class = TenantForm
    model = Tenant

    def form_valid(self, form):
        # import pdb; pdb.set_trace()
        self.object = form.save(commit=False)
        fail_url = 'tenant_edit'
        success_url = 'tenant_list'
        existing_profiles = Profile.objects.filter(tenant=self.object)

        ### Update both on local and Bambora ###
        if existing_profiles:
            for profile in existing_profiles:
                data = { 
                    "billing": {
                        "name": self.object.full_name, 
                        "phone_number": self.object.phone,
                        "email_address": self.object.email,
                    }
                }

                payload = {
                    'profiles_api_key': settings["profiles_api_key"],
                    'sub_merchant_id': profile.landlord.sub_merchant_id,
                    'customer_code': profile.customer_code,
                    'data': data
                }
                
                response = update_profile(payload)

                if response.status_code != 200:
                    message = "Error: {}".format(response.json())
                    messages.info(self.request, message)
                    return redirect(fail_url, pk=self.object.id)

            self.object = form.save()
            return super().form_valid(form)
        else:
            ### Update local only (does not exist on Bambora)###
            self.object = form.save()
            return super().form_valid(form)

class TenantCreateView(LoginRequiredMixin,CreateView):
    login_url = '/accounts/login/'
    form_class = TenantForm
    model = Tenant
    template_name = "tenant_form.html"
    success_url = reverse_lazy('tenant_list')

class TenantDeleteView(LoginRequiredMixin,DeleteView):
    login_url = '/accounts/login/'
    model = Tenant
    success_url = reverse_lazy('tenant_list')

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        success_url = self.get_success_url()
        fail_url = 'tenant_delete'
        existing_profiles = Profile.objects.filter(tenant=self.object)
        # import pdb; pdb.set_trace()

        ### Delete both on local and Bambora ###
        if existing_profiles:
            for profile in existing_profiles:
                payload = {
                    'profiles_api_key': settings["profiles_api_key"],
                    'sub_merchant_id': profile.landlord.sub_merchant_id,
                    'customer_code': profile.customer_code,
                }

                response = delete_profile(payload)

                if response.status_code != 200:
                    message = "Error: {}".format(response.json())
                    messages.info(self.request, message)
                    return redirect(fail_url, pk=self.object.id)

            self.object.delete()
            return HttpResponseRedirect(success_url)

        ### Delete on ocal only (does not exist on Bambora)###
        else:
            self.object.delete()
            return HttpResponseRedirect(success_url)


        # if bookings:
        #     ### Delete both on local and Bambora ###
        #     for booking in bookings:

        #         payload = {
        #             'profiles_api_key': settings["profiles_api_key"],
        #             'sub_merchant_id': Landlord.objects.get(apartment=booking.apartment).sub_merchant_id,
        #             'customer_code': booking.customer_code,
        #         }

        #         response = delete_profile(payload)
        
        #         if response.status_code != 200:
        #             message = "Error: {}".format(response.json())
        #             messages.info(self.request, message)
        #             return redirect(fail_url, pk=self.object.id)

        #     self.object.delete()
        #     return HttpResponseRedirect(success_url)
        # else:
        #     ### Delete on ocal only (does not exist on Bambora)###
        #     self.object.delete()
        #     return HttpResponseRedirect(success_url)

class BankAccountUpdateView(LoginRequiredMixin, UpdateView):
    login_url = '/accounts/login/'
    form_class = BankAccountForm
    model = Tenant
    template_name = "tenant_form.html"
    success_url = reverse_lazy('tenant_list')


    def form_valid(self, form):
        
        fail_url = 'bank_account_update'
        # import pdb; pdb.set_trace()
        existing_profiles = Profile.objects.filter(tenant=self.object)
        if existing_profiles:
            for profile in existing_profiles:
                data = {
                    "bank_account": {
                        "bank_account_holder": self.request.POST.get("bank_account_holder"),
                        "account_number": self.request.POST.get("account_number"),
                        "bank_account_type": "CA",
                        "institution_number": self.request.POST.get("institution_number"),
                        "branch_number": self.request.POST.get("branch_number")
                    },
                }
                
                payload = {
                    'profiles_api_key': settings["profiles_api_key"],
                    'sub_merchant_id': profile.landlord.sub_merchant_id,
                    'customer_code': profile.customer_code,
                    'data': data
                }

                response = update_profile(payload)
                
                if response.status_code == 200:
                    self.object = form.save()
                    return super().form_valid(form)
                else:
                    # import pdb; pdb.set_trace()
                    message = "Error: {}".format(response.json())
                    messages.info(self.request, message)
                    return redirect(fail_url, pk=self.object.id)
        else:
            self.object = form.save()
            return super().form_valid(form)

class CreditCardUpdateView(LoginRequiredMixin, UpdateView):
    login_url = '/accounts/login/'
    form_class = CreditCardForm
    model = Tenant
    template_name = "tenant_form.html"
    success_url = reverse_lazy('tenant_list')

    def form_valid(self, form):
        fail_url = 'credit_card_update'
        # import pdb; pdb.set_trace()
        existing_profiles = Profile.objects.filter(tenant=self.object)
        if existing_profiles:
            for profile in existing_profiles:
                data = {
                    "card": {
                        "name": self.request.POST.get("card_name"),
                        "expiry_month": self.request.POST.get("card_expiry_month"),
                        "expiry_year": self.request.POST.get("card_expiry_year"),
                    },
                }

                payload = {
                    'profiles_api_key': settings["profiles_api_key"],
                    'sub_merchant_id': profile.landlord.sub_merchant_id,
                    'customer_code': profile.customer_code,
                    'data': data
                }

                import pdb; pdb.set_trace()
                response = update_card(payload)
                
                if response.status_code == 200:

                    self.object.card_number = "••••••••••••"
                    self.object.card_cvd = "•••"
                    self.object = form.save()
                    return super().form_valid(form)
                else:
                    # import pdb; pdb.set_trace()
                    message = "Error: {}".format(response.json())
                    messages.info(self.request, message)
                    return redirect(fail_url, pk=self.object.id)
        else:
            self.object = form.save()
            return super().form_valid(form)





# BOOKING

class BookingListView(LoginRequiredMixin,ListView):
    login_url = '/accounts/login/'
    context_object_name = 'bookings'
    model = Booking

    def get_context_data(self,**kwargs):
        context = super(BookingListView,self).get_context_data(**kwargs)
        context['count_booking'] = Booking.objects.all().count()
        return context

class BookingCreateView(LoginRequiredMixin,CreateView):
    login_url = '/accounts/login/'
    form_class = BookingForm
    model = Booking

    def form_valid(self, form):
        # import pdb; pdb.set_trace()
        fail_url = 'booking_new'
        success_url = 'booking_list'
        self.object = form.save(commit=False)
        bookings = Booking.objects.filter(tenant=self.object.tenant)
        form_landlord = Landlord.objects.get(apartment=self.object.apartment)


        ### Re-use existing profile ###
        existing_profile = Profile.objects.filter(tenant=self.object.tenant, landlord=self.object.apartment.landlord)
        if existing_profile:
            self.object.profile = existing_profile[0]
            self.object.save()
            return redirect(success_url)

        #### Otherwise, create a new profile ####
        # import pdb; pdb.set_trace()
        data = {
            "billing": {
                "name": self.object.tenant.full_name, 
                "phone_number": self.object.tenant.phone,
                "email_address": self.object.tenant.email,
            }
        }

        bank_account= {
            "bank_account_holder": self.object.tenant.bank_account_holder,
            "account_number": self.object.tenant.account_number,
            "bank_account_type": self.object.tenant.bank_account_type,
            "institution_number": self.object.tenant.institution_number,
            "branch_number": self.object.tenant.branch_number
        }

        if not has_empty_fields(bank_account):
            data['bank_account'] = bank_account


        payload = {
            'profiles_api_key': settings["profiles_api_key"],
            'sub_merchant_id': Landlord.objects.get(apartment=self.object.apartment).sub_merchant_id,
            'data': data
        }

        response = create_profile(payload)

        if response.status_code == 200:
            # import pdb; pdb.set_trace()
            new_profile = Profile(customer_code=response.json()['customer_code'],
                tenant=self.object.tenant, 
                landlord=self.object.apartment.landlord)
            new_profile.save()
            self.object.profile = new_profile
            self.object.save()
            
            return redirect(success_url)
        else:
            # import pdb; pdb.set_trace()
            message = "Error: {}".format(response.json())
            messages.info(self.request, message)
            return redirect(fail_url)



        #     print('Create a new profile Return')
        # else:
        #     print('Create a new profile Return')
                # Landlord.objects.get(apartment=Booking.objects.get(tenant=self.object.tenant.apartment)

        # import pdb; pdb.set_trace()



        # import pdb; pdb.set_trace()

        
        # data = { 
        #     "billing": {
        #         "name": self.object.tenant.full_name, 
        #         "phone_number": self.object.tenant.phone,
        #         "email_address": self.object.tenant.email,
        #     }
        # }
        
        # payload = {
        #     'profiles_api_key': settings["profiles_api_key"],
        #     'sub_merchant_id': Landlord.objects.get(apartment=self.object.apartment).sub_merchant_id,
        #     'data': data
        # }

        # response = create_profile(payload)

        # if response.status_code == 200:
        #     self.object.customer_code = response.json()['customer_code']
        #     self.object.save()
        #     return redirect(success_url)
        # else:
        #     # import pdb; pdb.set_trace()
        #     message = "Error: {}".format(response.json())
        #     messages.info(self.request, message)
        #     return redirect(fail_url)

class BookingUpdateView (LoginRequiredMixin,UpdateView):
    login_url = '/accounts/login/'
    form_class = BookingForm
    model = Booking

    # def form_valid(self, form):
    #     self.object = form.save(commit=False)
    #     fail_url = 'update_profile'
    #     success_url = 'booking_list'

    #     import pdb; pdb.set_trace()

    #     data = { 
    #         "billing": {
    #             "name": self.object.tenant.full_name, 
    #             "address_line1": self.object.tenant.address,
    #             "city": self.object.tenant.city,
    #             "province": self.object.tenant.province,
    #             "country": self.object.tenant.country,
    #             "postal_code": self.object.tenant.postal_code,
    #             "phone_number": self.object.tenant.phone,
    #             "email_address": self.object.tenant.email,
    #         }
    #     }

    #     payload = {
    #         'profiles_api_key': settings["profiles_api_key"],
    #         'sub_merchant_id': Landlord.objects.get(apartment=self.object.apartment).sub_merchant_id,
    #         'customer_code': self.object.customer_code,
    #         'data': data
    #     }
        
    #     response = update_profile(payload)

    #     if response.status_code == 200:
    #         self.object = form.save()
    #         return super().form_valid(form)
    #     else:
    #         message = "Error: {}".format(response.json())
    #         messages.info(self.request, message)
    #         return redirect(fail_url, pk=self.object.id) 


# BATCH

class BatchListView(LoginRequiredMixin,TemplateView):
    login_url = '/accounts/login/'
    context_object_name = 'batches'
    template_name = 'crm/batch_list.html'
    model = Batch

    def get_context_data(self,**kwargs):
        
        context = super().get_context_data(**kwargs)
        batches = Batch.objects.all()
        ### Deconstruct each object in the query set to add the total
        query_set = []
        for batch in batches:
            total = 0
            for payment in batch.payment_set.all():
                total = total + payment.amount
            # import pdb; pdb.set_trace()
            batch_obj = {
                'created': batch.created,
                'process_date': batch.process_date,
                'landlord': batch.landlord,
                'id':batch.id,
                'confirmation_code': batch.confirmation_code,
                'status': batch.status,
                'total': total
            }
            query_set.append(batch_obj)
   
        context['batches'] = query_set
    
        return context

class BatchDetailsView(LoginRequiredMixin, TemplateView):

    login_url = '/accounts/login/'
    context_object_name = 'payments'
    model = Payment
    template_name = 'crm/batch_details.html'

    def get_context_data(self,**kwargs):
        # import pdb; pdb.set_trace()
        context = super(BatchDetailsView,self).get_context_data(**kwargs)
        context['payments'] = Batch.objects.get(id=kwargs['pk']).payment_set.all()
        context['batch'] = Batch.objects.get(id=kwargs['pk'])
        return context

# def create_batch(request):
    
#     submitted = False
#     if request.method == 'POST':
#         form = BatchForm(request.POST)
#         if form.is_valid():
#             data = form.cleaned_data
#             import pdb; pdb.set_trace()

#             process_date = data['process_date']
#             landlord = Landlord.objects.get(id=data['landlord'])
#             bookings = data['booking']
     

#             new_batch = Batch(
#                 process_date=process_date,
#                 landlord=landlord,
#                 batch_id='',
#                 status='Pending'
#             )
#             new_batch.save()

#             for pk in bookings:
#                 booking = Booking.objects.get(id=pk)
#                 new_payment = Payment(
#                     booking=booking,
#                     batch = new_batch,
#                     amount = booking.rent,
#                     payment_type = data['payment_type'],
#                     payment_method = 'DD'
#                 )
#                 new_payment.save()            

#             return HttpResponseRedirect('/batch/')
#     else:
#         form = BatchForm()
#         if 'submitted' in request.GET:
#             submitted = True


    # return render(request, 'crm/batch_form.html', {'form': form})
    # return render(request, 'crm/batch_form.html', {'form': form, 'submitted': submitted})

def create_batch(request):
    

    if request.method == 'POST':
        form = BatchForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            landlord = Landlord.objects.get(id=data['landlord'])
    
            new_batch = Batch(
                process_date=data['process_date'],
                landlord=landlord,
                confirmation_code='',
                status='Pending'
            )
            new_batch.save()

            for pk in data['tenant']:
                tenant = Tenant.objects.get(id=pk)
                ### Use the rent amount from the 'In-tenancy' booking common to this landlord and tenant, or default to $0.
                active_booking = Booking.objects.filter(apartment__landlord=landlord).filter(tenant=tenant).filter(status='In-tenancy').first()
                amount = active_booking.rent if active_booking else 0

                new_payment = Payment(
                    tenant = tenant,
                    batch = new_batch,
                    amount = amount,
                    payment_type = data['payment_type'],
                    payment_method = 'DD'
                )

            
            # for pk in data['booking']:
            #     booking = Booking.objects.get(id=pk)
            #     new_payment = Payment(
            #         booking=booking,
            #         batch = new_batch,
            #         amount = booking.rent,
            #         payment_type = data['payment_type'],
            #         payment_method = 'DD'
            #     )
                import pdb; pdb.set_trace() 
                new_payment.save()            

            # return HttpResponseRedirect('/batch/')
            return redirect('batch_list')
            # return render(request,'crm/batch_form.html', {'form': form})
    else:
        form = BatchForm()
        # if 'submitted' in request.GET:
        #     submitted = True

    return render(request, 'crm/batch_form.html', {'form': form})


def send_batch(request, pk):
    success_url='batch_list'
    fail_url = 'batch_review'
    batch = Batch.objects.get(id=pk)
    payments = batch.payment_set.all()
    process_date = str(batch.process_date).translate(str.maketrans('', '', string.punctuation))
    sub_merchand_id = batch.landlord.sub_merchant_id
    file_name = "Batch {} ({})".format("{0:0=4d}".format(batch.id), batch.process_date.strftime("%b %Y"))

    transactions = []
    for payment in payments:
        payment_type = payment.payment_type
        customer_code = payment.booking.profile.customer_code
        amount = payment.amount.__str__()[2:].translate(str.maketrans('', '', string.punctuation))
        address = payment.booking.apartment.address 
        street_no = str([int(s) for s in address.split() if s.isdigit()][0])
        unit_no = payment.booking.apartment.unit
        reference = '{}-{}'.format(unit_no, street_no) if unit_no else street_no

        # transaction_data = ['E', payment_type,'','','', amount,'','', customer_code, reference]
        transaction_data = ['E', payment_type,'','','', amount, reference,'', customer_code]

        transactions.append(transaction_data)

    payload = {
        'batch_api_key': settings["batch_api_key"],
        'process_date' : process_date,
        'sub_merchant_id' : sub_merchand_id,
        'filename' : file_name,
        'transactions': transactions
    }
    
    
    response = create_eft_batch(payload)

    if response.status_code == 200:
        batch_id = response.json()['batch_id']
        batch.confirmation_code = batch_id
        batch.status = 'Sent'
        # import pdb; pdb.set_trace()
        batch.save()

        # payload_2 = {
        #     'merchant_id': settings["merchant_id"],
        #     'report_passcode': settings["report_passcode"],
        #     'batch_id': response.json()['batch_id'],
        #     'sub_merchant_id': sub_merchand_id,
        # }

        # response_2 = get_batch(payload_2)

        # if response_2.status_code == 200:
        #     import pdb; pdb.set_trace()
        #     batch_report = response_2.json()
        #     batch.settlement_date = batch_report['response']['record'][0]['settlementDate']
        #     batch.save()

        return redirect(success_url)
    else:
        message = "Error: {}".format(response.json())
        messages.info(request, message)
        return redirect(fail_url, pk=pk)



class BatchDeleteView(LoginRequiredMixin, DeleteView):
    login_url = '/accounts/login/'
    model = Batch
    success_url = reverse_lazy('batch_list')

class PaymentUpdateView(LoginRequiredMixin,UpdateView):
    login_url = '/accounts/login/'
    form_class = PaymentForm
    model = Payment
    # success_url = reverse_lazy('batch_details')
    def get_success_url(self):
        # import pdb; pdb.set_trace()
        batch_id=self.object.batch.id
        return reverse_lazy('batch_review', kwargs={'pk': batch_id})








 # class BookingDeleteView(LoginRequiredMixin,DeleteView):
#     login_url = '/accounts/login/'
#     model = Booking
#     success_url = reverse_lazy('booking_list')

#     def delete(self, request, *args, **kwargs):
#         import pdb; pdb.set_trace()
#         self.object = self.get_object()
#         success_url = self.get_success_url()
#         fail_url = 'booking_delete'

#         payload = {
#             'profiles_api_key': settings["profiles_api_key"],
#             'sub_merchant_id': Landlord.objects.get(apartment=self.object.apartment).sub_merchant_id,
#             'customer_code': self.object.profile.customer_code,
#         }

#         response = delete_profile(payload)
        
#         if response.status_code == 200:
#             self.object.delete()
#             return redirect(success_url)
#         else:
#             message = "Error: {}".format(response.json())
#             messages.info(self.request, message)
#             return redirect(fail_url, pk=self.object.id)  


## Next steps:
#1) Once a booking is created, we should not be able to change the apartment
#2) Add credit card form (add, update, delete)
#3) Add bank account form (add, update)



# BAMBORA TESTS

# def ProfileUpdateView(request, pk):
#     # import pdb; pdb.set_trace()
#     profile = get_profile(pk)
#     context = profile

#     def post(self, request, *args, **kwargs):
#         form = ProfileForm(request.POST)
#         if form.is_valid():
#             return redirect('batch_list')

#     return render(request,'crm/profile_form.html', context)







# def load_batch_form(request):
#     form = BatchForm()
#     return render(request, 'crm/batch_form.html', {'form': form})





# def save_batch_form(request):
#     # import pdb; pdb.set_trace()
#     if request.method == "POST":

# def load_profile_form(request, pk=None):
#     # import pdb; pdb.set_trace()
#     response = get_profile(pk)
#     if response['code'] == 15: ## Does not exist
#         form = ProfileForm()
#     else: 
#         form = ProfileForm({
#             'customer_code': pk,
#             'language': response['language'],
#             'billing_name': response['billing']['name'],
#             'billing_address_line1' : response['billing']['address_line1'],
#             'billing_address_line2' : response['billing']['address_line2'],
#             'billing_city' : response['billing']['city'],
#             'billing_province' : response['billing']['province'],
#             'billing_country' : response['billing']['country'],
#             'billing_postal_code' : response['billing']['postal_code'],
#             'billing_phone_number' : response['billing']['phone_number'],
#             'billing_email_address' : response['billing']['email_address'],
#         })
    
#     return render(request, 'crm/profile_form.html', {'form': form})



# def load_bank_account_form(request, pk):
#     # import pdb; pdb.set_trace()
#     profile = Profile.objects.get(customer_code=pk)
#     account = BankAccount.objects.get(customer_code=profile)

#     if len(account.bank_account_holder) == 0:
#         print("new field")
#         form = BankAccountForm(initial={'customer_code': pk})

#     else:
#         print("existing record")
#         form = BankAccountForm({
#             'customer_code': pk,
#             'bank_account_holder' : account.bank_account_holder,
#             'bank_account_type' : account.bank_account_type,
#             'bank_institution_number' : account.institution_number,
#             'bank_branch_number' : account.branch_number,
#             'bank_account_number' : account.account_number,
#         })
    
#     return render(request, 'crm/bank_account_form.html', {'form': form})


# def save_profile_form(request):
#     # import pdb; pdb.set_trace()
#     if request.method == "POST":
        
#         if "billing_name" in request.POST: ### Profile Form
#             url = '/bambora/profile/{}/edit/details'.format(request.POST['customer_code'])
#             data = { 
#                 "language": request.POST.get("language"),
#                 "comments":"", 
#                 "billing": {
#                     "name": request.POST.get("billing_name"), 
#                     "address_line1": request.POST.get("billing_address_line1"),
#                     "address_line2": request.POST.get("billing_address_line2"),
#                     "city": request.POST.get("billing_city"),
#                     "province": request.POST.get("billing_province"),
#                     "country": request.POST.get("billing_country"),
#                     "postal_code": request.POST.get("billing_postal_code"),
#                     "phone_number": request.POST.get("billing_phone_number"),
#                     "email_address": request.POST.get("billing_email_address")
#                 }
#             }

         
#         elif "card_name" in request.POST: ### Credit Card Form
#             url = '/bambora/profile/{}/edit/credit-card'.format(request.POST['customer_code'])
#             data = {
#                 "card": {
#                     "name": request.POST.get("card_name"),
#                     "number": request.POST.get("card_number"),
#                     "expiry_month": request.POST.get("card_expiry_month"),
#                     "expiry_year": request.POST.get("card_expiry_year"),
#                     "cvd": request.POST.get("card_cvd")
#                 },
#             }

#         elif "bank_account_holder" in request.POST: ### Bank Account
#             url = '/bambora/profile/{}/edit/bank-account'.format(request.POST['customer_code'])
#             data = {
#                 "bank_account": {
#                     "bank_account_holder": request.POST.get("bank_account_holder"),
#                     "account_number": request.POST.get("bank_account_number"),
#                     "bank_account_type": "CA",
#                     "institution_number": request.POST.get("bank_institution_number"),
#                     "branch_number": request.POST.get("bank_branch_number")
#                 },
#             }

#         payload = {
#             'profiles_api_key': settings["profiles_api_key"],
#             'customer_code': request.POST.get("customer_code"),
#             'data': data
#         }

#         attempt_num = 0  # keep track of how many times we've retried
#         while attempt_num < 2:

#             response = update_or_create_profile(payload)
#             # import pdb; pdb.set_trace()

#             if response.status_code == 200:
#                 ## If CREATE on the profile form
#                 if response.request.method == 'POST': 
#                     customer_code = response.json()['customer_code']
#                     ## Create new profile using Bambora's customer code
#                     new_profile = Profile(customer_code = customer_code)
#                     new_profile.save()
#                     ## Create new bank account using new profile
#                     new_account = BankAccount(customer_code=new_profile)
#                     new_account.save()
#                 ### If UPDATE on the bank account form
#                 if response.request.method == 'PUT' and "bank_account_holder" in request.POST:
#                     customer_code = response.json()['customer_code']
#                     updated_profile = Profile.objects.get(customer_code=customer_code)
#                     updated_bank_account = BankAccount.objects.get(customer_code=updated_profile)
#                     updated_bank_account.bank_account_holder = request.POST.get("bank_account_holder")
#                     updated_bank_account.account_number = request.POST.get("bank_account_number")
#                     updated_bank_account.bank_account_type = "CA"
#                     updated_bank_account.institution_number = request.POST.get("bank_institution_number")
#                     updated_bank_account.branch_number = request.POST.get("bank_branch_number")
#                     updated_bank_account.save()

#                 return redirect('/bambora/profile/list')
#             else:
#                 attempt_num += 1
#                 time.sleep(0)  # Wait for 5 seconds before re-trying

#         # import pdb; pdb.set_trace()
#         message = "Error: {}".format(response.json()['details'][0]['message'])
#         messages.info(request, message)
#         return HttpResponseRedirect(url)

#     else:
#         # return Response({"error": "Method not allowed"}, status=status.HTTP_400_BAD_REQUEST)
#         return HttpResponse({"error": "Method not allowed"})




# def profile_list_view(request):
#     import pdb; pdb.set_trace()
#     profile_list = Profile.objects.all()
#     arr = []
#     for profile in profile_list:
#         profile_data = get_profile(profile.customer_code)
#         arr.append(profile_data)
#     # import pdb; pdb.set_trace()
#     return render(request,'crm/profile_list.html', {'profile_list': arr})

# def BatchListView(request):
#     # batch = get_batch(settings['merchant_id'], settings['report_passcode'], filter_type='batch_id', filter_value='10000014')
#     batch = get_batch(settings['merchant_id'], settings['report_passcode'], filter_type='', filter_value='')
#     # import pdb; pdb.set_trace()
#     context = batch
#     return render(request,'crm/batch_list.html', context)

# def BatchDetailView(request):
#     return render(request,'crm/tenant_list.html', context)

# # def create_new_profile(request):
# def load_profile_form2(request, pk=None):
#     # import pdb; pdb.set_trace()
#     # response = get_profile(pk)
#     # if response['code'] == 15: ## Does not exist
#     #     form = ProfileForm()
#     # else: 
#     #     form = ProfileForm({
#     #         'customer_code': pk,
#     #         'language': response['language'],
#     #         'billing_name': response['billing']['name'],
#     #         'billing_address_line1' : response['billing']['address_line1'],
#     #         'billing_address_line2' : response['billing']['address_line2'],
#     #         'billing_city' : response['billing']['city'],
#     #         'billing_province' : response['billing']['province'],
#     #         'billing_country' : response['billing']['country'],
#     #         'billing_postal_code' : response['billing']['postal_code'],
#     #         'billing_phone_number' : response['billing']['phone_number'],
#     #         'billing_email_address' : response['billing']['email_address'],
#     #     })
#     if request.method == "GET":
#         form = ProfileForm()
    
#     return render(request, 'crm/profile_form.html', {'form': form})


# def delete_profile_view(request, code):

#     import pdb; pdb.set_trace()
#     if request.method == "GET":

#         pk = Profile.objects.filter(customer_code=code)[0].id
#         attempt_num = 0

#         while attempt_num < 2:

#             response = delete_profile(code)
            
#             if response.status_code == 200:
#                 Profile.objects.get(id=pk).delete()
#                 return redirect('/bambora/profile/list2')
#             else:
#                 attempt_num += 1
#                 time.sleep(0)

#         # import pdb; pdb.set_trace()
#         message = "Error: {}".format(response.json()['details'][0]['message'])
#         messages.info(request, message)
#         return HttpResponseRedirect(url)

#     else:
#         return HttpResponse({"error": "Method not allowed"})







###########################################################################


def profile_list_view2(request):
    # import pdb; pdb.set_trace()
    profiles = Profile.objects.all()
    return render(request,'crm/profile_list2.html', {'profiles': profiles})












# class ProfileUpdateView(LoginRequiredMixin, UpdateView):
#     login_url = '/accounts/login/'
#     form_class = ProfileForm2
#     model = Profile
#     template_name = "profile_form2.html"
#     success_url = reverse_lazy('profile_list2')

#     ### Send updated data through API endpoint
#     def form_valid(self, form):

#         fail_url = 'update_profile'
#         success_url = 'profile_list2'

#         bookings = Booking.objects.filter(profile=self.object)

#         if bookings:

#             for booking in bookings:
#                 import pdb; pdb.set_trace()
#                 # customer_code = booking.customer_code
#                 # sub_merchant = 
#                 print(booking)

#             print('Update bambora')

#             import pdb; pdb.set_trace()

#             data = { 
#                 "billing": {
#                     "name": self.object.full_name, 
#                     "address_line1": self.object.address,
#                     "city": self.object.city,
#                     "province": self.object.province,
#                     "country": self.object.country,
#                     "postal_code": self.object.postal_code,
#                     "phone_number": self.object.phone,
#                     "email_address": self.object.email,
#                 }
#             }

#             payload = {
#                 'profiles_api_key': settings["profiles_api_key"],
#                 'sub_merchant_id': settings["sub_merchant_id"],
#                 'customer_code': self.object.customer_code,
#                 'data': data
#             }
            
#             response = update_profile(payload)

#             if response.status_code == 200:
#                 self.object = form.save()
#                 return super().form_valid(form)
#             else:
#                 message = "Error: {}".format(response.json())
#                 messages.info(self.request, message)
#                 return HttpResponseRedirect(fail_url, pk=self.object.id) 

#         else:
#             print('Update only local')
#             self.object = form.save()
#             return super().form_valid(form)

# class ProfileDeleteView(LoginRequiredMixin, DeleteView):
#     login_url = '/accounts/login/'
#     model = Profile
#     success_url = reverse_lazy('profile_list2')

#     def delete(self, request, *args, **kwargs):
#         # import pdb; pdb.set_trace()
#         self.object = self.get_object()
#         success_url = self.get_success_url()
#         fail_url = '/bambora/profile/{}/delete'.format(self.object.id)

#         payload = {
#             'profiles_api_key': settings["profiles_api_key"],
#             'sub_merchant_id': settings["sub_merchant_id"],
#             'customer_code': self.object.customer_code,
#         }

#         response = delete_profile(payload)
        
#         if response.status_code == 200:
#             self.object.delete()
#             return HttpResponseRedirect(success_url)
#         else:
#             message = "Error: {}".format(response.json())
#             messages.info(self.request, message)
#             return HttpResponseRedirect(fail_url)  

# def load_credit_card_form(request, pk):
#     # import pdb; pdb.set_trace()
#     instance = Profile.objects.get(id=pk)

#     payload = {
#         'profiles_api_key': settings["profiles_api_key"],
#         'sub_merchant_id': settings["sub_merchant_id"],
#         'customer_code': instance.customer_code,
#     }

#     response = get_profile(payload)

#     if len(response['card']['name']) == 0:
#         form = CreditCardForm(initial={
#             'customer_code': instance.customer_code,
#             'is_new': True
#         })
#     else:
#         form = CreditCardForm(initial={
#             'customer_code': instance.customer_code,
#             'is_new': False,
#             'card_name' : response['card']['name'],
#             'card_number' : response['card']['number'],
#             'card_number' : response['card']['number'].replace('X','•'),
#             'card_expiry_month' : response['card']['expiry_month'],
#             'card_expiry_year' : response['card']['expiry_year'],
#             'card_cvd' : '•••',
#         })

#     context = {
#         'form': form,
#         'pk': pk
#     }

#     return render(request, 'crm/credit_card_form.html', context)

# def submit_credit_card_form(request, pk):
#     fail_url = 'load_credit_card'
#     success_url = 'profile_list2'
    
#     if request.method == "POST":
#         # import pdb; pdb.set_trace()
#         is_new = str_to_bool(request.POST.get("is_new"))

#         data = {
#             "card": {
#                 "name": request.POST.get("card_name"),
#                 "number": request.POST.get("card_number"),
#                 "expiry_month": request.POST.get("card_expiry_month"),
#                 "expiry_year": request.POST.get("card_expiry_year"),
#                 "cvd": request.POST.get("card_cvd")
#             }
#         }

#         payload = {
#             'profiles_api_key': settings["profiles_api_key"],
#             'sub_merchant_id': settings["sub_merchant_id"],
#             'customer_code': request.POST.get("customer_code")
#         }

#         if is_new:
#             payload['data'] = data
#             response = create_card(payload)
#         else:
#             del data['card']['number']
#             del data['card']['cvd']
#             payload['data'] = data
#             response = update_card(payload)

#         if response.status_code == 200:
#             return redirect(success_url)
#         else:
#             message = "Error: {}".format(response.json())
#             messages.info(request, message)
#             return redirect(fail_url, pk=pk)   
#     else:
#         messages.info(request, "Error: Method not allowed")
#         return redirect(fail_url, pk=pk)  

# def delete_credit_card(request, pk):
#     # import pdb; pdb.set_trace()
#     payload = {
#         'profiles_api_key': settings["profiles_api_key"],
#         'sub_merchant_id': settings["sub_merchant_id"],
#         'customer_code': Profile.objects.get(id=pk).customer_code,
#         'data': {}
#     }
#     delete_card(payload)
#     return redirect('load_credit_card',pk=pk)





### Exceptions and error handling with codes (if code, do that)
### URL path relative (ie, url name) rather than absolute in all views
## For reporting, I need the passcode straight from dashboard, not encoded.
## For recurring, I need the default passcoded, not the encoded.


## Which comes first: the booking or the profile? 
## The profile comes with a customer code. But a booking (via booking<-->apartment<-->landlord) comes with the landlord's sub-merchant id.
## I need a sub-merchant id to create a profile.

## One profile must have multiple customer codes if bookings with multiple landlords. 
# If today, booking with Andres: customer code X. One year later, booking with Steve: customer code Y; yet, same profile.
# Realistically, since tenant has one booking at a time, then each profile has one customer code. Customer code can change over time.
# What happens then if tenant who current stays with Andres, wants to book a room with Steve? Bookings are not necessarily overlapping, but you still have 2 bookings at the same time.
# This means the profile must be able to have 2 customer codes + at the same time.

#In other words, each booking should have its own customer code; NOT each profile.