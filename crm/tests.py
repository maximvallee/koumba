import requests
import json
import time
# from bambora.config import settings

def convert_lists_to_str(arr_list):
    data = ''       
    for j in arr_list:
        arr_str = ','.join(j)
        data = data + arr_str +'\r\n'
    return data

def create_eft_batch(process_date, sub_merchant, file_name, transactions):

    url = 'https://api.na.bambora.com/v1/batchpayments'
    data = "--CHEESE\r\nContent-Disposition: form-data; name=\"criteria\"\r\nContent-Type: application/json\r\n\r\n{{\r\n    \"process_date\": \"{}\",\r\n    \"sub_merchant_id\": \"{}\"\r\n}}\r\n--CHEESE\r\nContent-Disposition: form-data; name=\"{}\"; filename=\"{}.csv\"\r\nContent-Type: text/plain\r\n\r\n{}--CHEESE--\r\n".format(process_date, sub_merchant, file_name, file_name, convert_lists_to_str(transactions))



    headers = {
    'Content-Type': 'multipart/form-data; boundary=CHEESE',
    # 'Authorization': 'Passcode {}'.format(settings["batch_api_key"]),
    # 'Authorization': 'Passcode MzAwMjA4ODM4OkM4YzVBZThGQUEyMjQ0ZjY5MmMwNzc5Y0ZBOUIwMzQ3', ### rentaplacenow_test
    'Authorization': 'Passcode MzcyMjEwMDAwOmI0QTA2QWQ2MjExMjQ0OTZCMEZCNjI1Q2Y5NjVlM0Mz', ### partner account (main)
    'FileType': 'STD',

    }

  
    response = requests.request("POST", url, headers=headers, data = data)
    print(response.json())
    import pdb; pdb.set_trace()

    return response


# eft_list = [
#                 ['E','C','001','99001','09400313371','10000','1000070001','Jonhn Cnezo'],
#                 ['E','C','002','99002','09400313372','20000','1000070002','Mike mOyer'],
#                 ['E','C','003','99003','09400313373','30000','1000070003','Don Stevens'],
#             ]

eft_list = [
    ['E','D','','','','35000','','','D050872f16f14b67b9d3DE40A44B61d3','10B-R'],
    ['E','D','','','','10000','','','AE2428E3F1ce4a938255D0AcEF4Fb37e','10A-R'],
]

create_eft_batch('20201130', '300208838', "baby_green2", eft_list)


        # data = { 
        #     "billing": {
        #         "name": self.object.tenant.full_name, 
        #         "phone_number": self.object.tenant.phone,
        #         "email_address": self.object.tenant.email,
        #     }
        # }
        
        # payload = {
        #     'profiles_api_key': settings["profiles_api_key"],
        #     'sub_merchant_id': Landlord.objects.get(apartment=self.object.apartment).sub_merchant_id,
        #     'data': data
        # }

