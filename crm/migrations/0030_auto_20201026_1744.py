# Generated by Django 2.0 on 2020-10-26 21:44

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0029_auto_20201026_1732'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='customercode',
            name='booking',
        ),
        migrations.RemoveField(
            model_name='profile',
            name='address',
        ),
        migrations.RemoveField(
            model_name='profile',
            name='city',
        ),
        migrations.RemoveField(
            model_name='profile',
            name='country',
        ),
        migrations.RemoveField(
            model_name='profile',
            name='email',
        ),
        migrations.RemoveField(
            model_name='profile',
            name='full_name',
        ),
        migrations.RemoveField(
            model_name='profile',
            name='phone',
        ),
        migrations.RemoveField(
            model_name='profile',
            name='postal_code',
        ),
        migrations.RemoveField(
            model_name='profile',
            name='province',
        ),
        migrations.AddField(
            model_name='booking',
            name='customer_code',
            field=models.ForeignKey(default='1', on_delete=django.db.models.deletion.CASCADE, to='crm.Profile'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='profile',
            name='customer_code',
            field=models.CharField(default='1', max_length=50),
            preserve_default=False,
        ),
        migrations.DeleteModel(
            name='CustomerCode',
        ),
    ]
