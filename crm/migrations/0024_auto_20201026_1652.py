# Generated by Django 2.0 on 2020-10-26 20:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0023_auto_20201026_1615'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='tenant',
            name='account_number',
        ),
        migrations.RemoveField(
            model_name='tenant',
            name='bank_account_holder',
        ),
        migrations.RemoveField(
            model_name='tenant',
            name='bank_account_type',
        ),
        migrations.RemoveField(
            model_name='tenant',
            name='branch_number',
        ),
        migrations.RemoveField(
            model_name='tenant',
            name='card_cvd',
        ),
        migrations.RemoveField(
            model_name='tenant',
            name='card_expiry_month',
        ),
        migrations.RemoveField(
            model_name='tenant',
            name='card_expiry_year',
        ),
        migrations.RemoveField(
            model_name='tenant',
            name='card_name',
        ),
        migrations.RemoveField(
            model_name='tenant',
            name='card_number',
        ),
        migrations.RemoveField(
            model_name='tenant',
            name='institution_number',
        ),
        migrations.AddField(
            model_name='booking',
            name='account_number',
            field=models.CharField(blank=True, max_length=12),
        ),
        migrations.AddField(
            model_name='booking',
            name='bank_account_holder',
            field=models.CharField(blank=True, max_length=50),
        ),
        migrations.AddField(
            model_name='booking',
            name='bank_account_type',
            field=models.CharField(blank=True, choices=[('CA', 'Canadian')], default='CA', max_length=2),
        ),
        migrations.AddField(
            model_name='booking',
            name='branch_number',
            field=models.CharField(blank=True, max_length=5),
        ),
        migrations.AddField(
            model_name='booking',
            name='card_cvd',
            field=models.CharField(blank=True, max_length=3),
        ),
        migrations.AddField(
            model_name='booking',
            name='card_expiry_month',
            field=models.CharField(blank=True, choices=[('', '--'), ('01', '01'), ('02', '02'), ('03', '03'), ('04', '04'), ('05', '05'), ('06', '06'), ('07', '07'), ('08', '08'), ('09', '09'), ('10', '10'), ('11', '11'), ('12', '12')], max_length=2),
        ),
        migrations.AddField(
            model_name='booking',
            name='card_expiry_year',
            field=models.CharField(blank=True, choices=[('', '--'), (20, 2020), (21, 2021), (22, 2022), (23, 2023), (24, 2024), (25, 2025), (26, 2026), (27, 2027), (28, 2028), (29, 2029), (30, 2030)], max_length=2),
        ),
        migrations.AddField(
            model_name='booking',
            name='card_name',
            field=models.CharField(blank=True, max_length=50),
        ),
        migrations.AddField(
            model_name='booking',
            name='card_number',
            field=models.CharField(blank=True, max_length=16),
        ),
        migrations.AddField(
            model_name='booking',
            name='institution_number',
            field=models.CharField(blank=True, max_length=3),
        ),
    ]
