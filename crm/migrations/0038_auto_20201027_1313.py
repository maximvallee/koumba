# Generated by Django 2.0 on 2020-10-27 17:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0037_auto_20201027_1112'),
    ]

    operations = [
        migrations.AlterField(
            model_name='landlord',
            name='sub_merchant_id',
            field=models.CharField(blank=True, max_length=9),
        ),
    ]
