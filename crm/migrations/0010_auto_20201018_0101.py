# Generated by Django 2.0 on 2020-10-18 05:01

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0009_auto_20201018_0051'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bankaccount',
            name='customer_code',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='crm.Profile'),
        ),
    ]
