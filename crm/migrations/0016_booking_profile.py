# Generated by Django 2.0 on 2020-10-24 04:31

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0015_auto_20201024_0022'),
    ]

    operations = [
        migrations.AddField(
            model_name='booking',
            name='profile',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='crm.Profile'),
            preserve_default=False,
        ),
    ]
