# Generated by Django 2.0 on 2020-10-18 04:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0007_auto_20201015_1912'),
    ]

    operations = [
        migrations.CreateModel(
            name='BankAccount',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('customer_code', models.CharField(max_length=50)),
                ('bank_account_holder', models.CharField(max_length=50)),
                ('bank_account_type', models.CharField(choices=[('CA', 'Canadian'), ('US', 'American')], default='CA', max_length=2)),
                ('account_number', models.CharField(max_length=12)),
            ],
        ),
    ]
