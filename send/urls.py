from django.urls import path
from send import views

app_name = 'send'

urlpatterns = [
    path('statement/<int:pk>',views.statement, name='statement'),
    path('reminder/<int:pk>',views.reminder, name='reminder'),
]
