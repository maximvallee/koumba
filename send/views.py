from django.shortcuts import render
from django.conf import settings
from django.core.mail import EmailMessage
from django.template.loader import get_template
from crm.models import Booking,Landlord,Tenant,Apartment,Settings
from django.core.mail import EmailMultiAlternatives
from third_project.utils import create_context


from datetime import datetime, date
from datetime import timedelta
import calendar

from django.db.models import Sum
from django.db.models import Q

from dateutil.rrule import *


# Create your views here.
def statement(request,pk):

    # CONFIGS
    subject = 'The rentroll has just been processed'
    email_from = 'Loop Management <propertymanagementloop@gmail.com>'
    recipient_list = [Landlord.objects.get(id=pk).email]
    bcc = ['Loop Management <propertymanagementloop@gmail.com>',]

    context = create_context(pk)
    html_content = get_template('send/statement_template.html').render(context)
    text_content = get_template('send/statement_template.txt').render(context)
    message = EmailMultiAlternatives(subject, text_content, email_from, recipient_list, bcc)
    message.attach_alternative(html_content,'text/html')
    message.send()

    return render(request,'send/statement_confirm_sent.html')



def reminder(request,pk):

    subject = 'Reminder: Your rent will be collected on the next business day'
    email_from = settings.EMAIL_HOST_USER
    recipient_list = [Booking.objects.get(id=pk).tenant.email,]
    bcc = [settings.EMAIL_HOST_USER,]
    tenant = Booking.objects.get(id=pk).tenant.name #Name of the tenant associated to this booking. Normal query (not reverse) so we use "." for multiple model levels
    landlord = Booking.objects.get(id=pk).apartment.landlord.name #Name of the landlord associated to this booking. Normal query (not reverse) so we use "." for multiple model levels

    def get_first_day(date):
        last_day = date.replace(day = calendar.monthrange(date.year, date.month)[1])
        first_day = last_day + timedelta(days=1)
        first_day = (first_day.strftime('%B %d'))
        return first_day

    context = {'tenant':tenant, 'first_day':get_first_day(datetime.now()), 'landlord':landlord}
    html_content = get_template('send/reminder_template.html').render(context)
    text_content = get_template('send/reminder_template.txt').render(context)
    message = EmailMultiAlternatives(subject, text_content, email_from, recipient_list, bcc)
    message.attach_alternative(html_content,'text/html')
    message.send()

    return render(request,'send/reminder_confirm_sent.html')
