import datetime
from datetime import timedelta
import calendar

# start = datetime.datetime(2019, 5, 26, 13, 30,)
# end = datetime.datetime(2019, 5, 26, 18, 30,)
# tdelta = datetime.timedelta(minutes=15)
#
#
# print(start + tdelta)

from datetime import datetime, timedelta

def datetime_range(start, end, delta):
    current = start
    while current < end:
        yield current
        current += delta

dts = [dt.strftime('%H:%M') for dt in
       datetime_range(datetime(2016, 9, 1, 10), datetime(2016, 9, 1, 13),
       timedelta(minutes=10))]

for j in dts:
    print (j)
# print(dts)
